import mook
from fastapi import FastAPI
from pydantic import BaseModel
from typing import List

app = FastAPI()


class InputData(BaseModel):
    arr: list

@app.post("/ordenamiento-palabras")
async def tu_endpoint(data: InputData):
    arr_sorted = sorted(data.arr)
    return {"arr": arr_sorted}
def main():
    my_documents = mook.my_documents
    sortedList = sorted(my_documents)

    for item in sortedList:
        print(item)


class InputData(BaseModel):
    numeros: List[int]

@app.post("/detectar-repetidos")
async def detectar_numeros_repetidos(data: InputData):
    contador = {}
    repetidos = []

    for numero in data.numeros:
        if numero in contador:
            contador[numero] += 1
            if contador[numero] == 2:
                repetidos.append(numero)
        else:
            contador[numero] = 1

    return {"Numeros repetidos": repetidos}
